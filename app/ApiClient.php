<?php

namespace App;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class ApiClient {

    protected $apiUrl;
    protected $client;
    protected $token;
    protected $secret;
    protected $headers = [];

    public function __construct($domain, $token)
    {
        $this->apiUrl = 'https://'.$domain.'.hiweb.io/api/';

        if (env('API_URL')) {
            $this->apiUrl = env('API_URL');
        }

        $this->client = new Client(['base_uri' => $this->apiUrl]);

        $this->token = $token;
        $this->secret = env('APP_SECRET');

        $this->headers = [
        ];

        if ($this->token) {
            $this->headers['OAuth-Token'] = $this->token;
        }

        if ($this->secret) {
            $this->headers['Hiweb-Secret'] = $this->secret;
        }
    }

    /**
    * Send api request
    *
    * @param string Request method
    * @param string Path
    * @param array Data
    * @return array|null
    */
    public function send($method, $path, $data = [], $tries = 3)
    {
        try {

            try {

                if ($tries) {
                    return $this->send($method, $path, $data, $tries - 1);
                }

                $response = $this->client->request($method, $path, [
                    'headers' => $this->headers,
                    'json' => $data
                ]);

            } catch (RequestException $e) {

                if ($e->hasResponse()) {

                    $error = @json_decode((string)$e->getResponse()->getBody(), true);

                    if (is_array($error)) {
                        return $error;
                    }
                }

                \Log::info('(0) API send request failed: ['.$method.']'.' '.$path.' ('.$e->getMessage().')');

                return null;

            }

            $data = json_decode($response->getBody(), true);

            if (is_array($data)) {
                return $data;
            }

            throw new \Exception('Cannot decode API data. '.$response->getBody());

        } catch (\Exception $e) {
            \Log::info('API send request failed: ['.$method.']'.' '.$path.' ('.$e->getMessage().')');
            return null;
        }
    }

    /**
    * Get api data
    *
    * @param string Path
    * @param array Query param
    * @return array|null
    */
    public function get($path, $params = [], $tries = 3)
    {

        try {

            try {

                $response = $this->client->request('GET', $path, [
                    'headers' => $this->headers,
                    'query' => $params
                ]);

            } catch (RequestException $e) {

                if ($tries) {
                    return $this->get($path, $params, $tries - 1);
                }

                if ($e->hasResponse()) {

                    $error = @json_decode((string)$e->getResponse()->getBody(), true);

                    if (is_array($error)) {
                        return $error;
                    }
                }

                \Log::info('(0) API get request failed: ['.$method.']'.' '.$path.' ('.$e->getMessage().')');

                return null;

            }

            $data = json_decode($response->getBody(), true);

            if (is_array($data)) {
                return $data;
            }

            throw new \Exception('Cannot decode API data');

        } catch (\Exception $e) {
            \Log::info('API get request failed: ['.$method.']'.' '.$path.' ('.$e->getMessage().')');
            return null;
        }
    }

    /**
    * Check if response is error
    *
    * @param void
    * @return bool
    */
    public function isError($data)
    {
        if (!$data or !is_array($data)) {
            return true;
        }

        if (isset($data['errors'])) {
            return true;
        }

        return false;
    }
}
