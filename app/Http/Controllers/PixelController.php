<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models as Model;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

use App\ApiClient;

use Exception;

class PixelController extends Controller
{
    public function get(Request $request)
    {

        $client = new Client();

        if ($request->nonce) {

            $response = $client->get(env('URL_OAUTH_TOKEN').$request->nonce);

            $appData = json_decode($response->getbody()->getContents());

            foreach ($appData->included as $shop) {
                if ($shop->id === $appData->data->relationships->shop->data->id) {
                    $shopData = $shop;
                }
            }

            $domain = $shopData->attributes->domain;

            try {

                $pixel = Model\Pixel::findOrFail('shop_id', $appData->data->attributes->shop_id)->first();

            } catch(Exception $e) {

                $pixel = new Model\Pixel;

                $pixel->shop_id = $appData->data->attributes->shop_id;
                $pixel->shop_domain = $domain;

            }

            $pixel->token = $appData->data->attributes->token;

            $pixel->save();

            return redirect('http://'.$domain.env('URL_WEBSITE'));

        } elseif ($request->app_token_id) {

            try {

                $response = $client->get(env('URL_APP_TOKEN_ID').$request->app_token_id);

                $appData = json_decode($response->getbody()->getContents());

                $request->session()->put($appData->data->attributes->shop_id, true);

                return redirect(route('shop.manage', ['id' => $appData->data->attributes->shop_id]));

            } catch (RequestException $e) {

                return abort(403, 'Invalid token');

            }


        } else {
            return 'App token id not found';
        }

    }

    public function make(Request $request, $id)
    {

        // Check permission - if this user can manage this shop
        if (!$request->session()->get($id)) {
            return abort(403, 'You dont have permission to access this shop');
        }

        $pixel = Model\Pixel::where('shop_id', $id)->orderBy('created_at', 'desc')->first();

        $pixel->pixel = $request->pixel;

        $apiClient = new ApiClient($pixel->shop_domain, $pixel->token);

        if (!empty($pixel->code)) {

            $codeId = $pixel->code->code_id;

        } else {
            $codeId = null;
        }

        $check = $apiClient->get('codes/'.$codeId);

        if (!empty($check['errors'])) {

            $method = 'POST';
            $path = 'codes';

        } else {

            $method = 'PATCH';
            $path = 'codes/'.$codeId;

        }

        $appData = $apiClient->send($method, $path, [
            'data' => [
                'type' => 'codes',
                'attributes' => [
                    'title' => 'Facebook Pixel',
                    'hook' => 'head',
                    'code' => $pixel->pixel
                ]
            ]
        ]);

        if (!empty($check['errors'])) {

            $code = new Model\Code;

            $code->code_id = $appData['data']['id'];
            $code->pixel_id = $pixel->id;
            $code->type = 'head';

            $code->save();

        }

        $pixel->save();

        return back();

    }

    public function shopManage(Request $request, $id)
    {

        // Check permission - if this user can manage this shop
        if (!$request->session()->get($id)) {
            return abort(403, 'You dont have permission to access this shop');
        }

        // Get data by shop id
        $pixel = Model\Pixel::where('shop_id', $id)->orderBy('created_at', 'desc')->firstOrFail();

        return view('controllers.home.index', compact('pixel'));

    }

}
