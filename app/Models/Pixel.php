<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pixel extends Model
{
    protected $table = 'pixels';

    public function code() {
        return $this->hasOne('App\Models\Code');
    }
}
