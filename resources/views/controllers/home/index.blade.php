@extends('layouts.default')

@section('content')

    <div class="pixel mt-5">

        <!-- <h6 class="heading-small text-muted mb-4">Add pixel code</h6> -->

        <div class="pl-lg-4">

            <form method="post" action="{{route('pixel.make', $pixel->shop_id)}}">
                @csrf

                <div class="row">

                    <div class="col">

                        <div class="form-group focused">
                            <label class="form-control-label" for="input-username">Pixel code</label>
                            <textarea name="pixel" type="text" id="input-username" class="form-control form-control-alternative" placeholder="Pixel code" rows="8">@if($pixel){{ $pixel->pixel }}@endif</textarea>
                        </div>

                    </div>

                </div>

                <button type="submit" class="btn btn-primary">Save</button>

            </form>

        </div>

    </div>

@endsection
