<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    @include('partials.head')

</head>
<body>

    <div class="main-content">

        <div class="container">

            @yield('content')

        </div>

        @include('partials.footer')

    </div>

</body>
</html>
