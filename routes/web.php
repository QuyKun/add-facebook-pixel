<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PixelController@get')->name('pixel.get');

Route::post('{id}', 'PixelController@make')->name('pixel.make');

Route::get('{id}', [
    'as' => 'shop.manage',
    'uses' => 'PixelController@shopManage'
]);
